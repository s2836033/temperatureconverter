import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TempServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Convertor convertor;

    public void init() throws ServletException{
        convertor = new Convertor();
    }
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius Fahrenheit Conversion";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in fahrenheit: " +
                Double.toString(convertor.convert(Double.parseDouble(request.getParameter("celsius")))) +
                "</BODY></HTML>");






    }
}
